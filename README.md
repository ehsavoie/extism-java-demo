# Java Extism Sample

> From https://github.com/thomasdarimont/extism-java-example

## First test

### Build the Jar file

```bash
LD_LIBRARY_PATH=/usr/local/lib mvn verify
```

### Download and execute the wasm plugin from Java

```bash
curl https://raw.githubusercontent.com/extism/extism/main/wasm/code.wasm > code.wasm
LD_LIBRARY_PATH=/usr/local/lib java -cp target/extism-java-*-with-dependencies.jar example.ExtismExample "code.wasm" count_vowels "Hello World"
```

## Second test: with TinyGo

### Build the wasm plugin

```bash
cd hello-world
tinygo build -o hello-world.wasm -target wasi main.go
```

### Execute the wasm plugin from Java

```bash
LD_LIBRARY_PATH=/usr/local/lib java -cp target/extism-java-*-with-dependencies.jar example.ExtismExample "hello-world/hello-world.wasm" helloWorld "Hey!"
```

## Third test: with Rust

```bash
cd hello-rust
cargo clean
cargo build --release --target wasm32-wasi
```

### Execute the wasm plugin from Java

```bash
LD_LIBRARY_PATH=/usr/local/lib java -cp target/extism-java-*-with-dependencies.jar example.ExtismExample "hello-rust/target/wasm32-wasi/release/hello_rust.wasm" add "{\"a\":10,\"b\":32}"
```

## Use another Java version

```bash
sdk install java 17.0.5-amzn
```

